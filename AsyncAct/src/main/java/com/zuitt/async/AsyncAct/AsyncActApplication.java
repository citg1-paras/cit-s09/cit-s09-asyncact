package com.zuitt.async.AsyncAct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@SpringBootApplication
@RestController
@RequestMapping("/greeting")

public class AsyncActApplication {

	private ArrayList<Student> students = new ArrayList<>();

	public static void main(String[] args) {
		SpringApplication.run(AsyncActApplication.class, args);
	}

	@GetMapping("/hello")
	public String hello(){
		return "Hello World";
	}

	@GetMapping("/hi")
	public String hi(@RequestParam(value="name", defaultValue = "John") String name){
		return String.format("Hi %s", name);
	}

	@GetMapping("/friend")
	public String friend(@RequestParam(value="name", defaultValue = "Joe") String name, @RequestParam(value="friend", defaultValue = "Jane") String friend){
		return  String.format("Hello %s! My name is %s.", friend, name);
	}

	@GetMapping("/hello/{name}")   public String greetFriend(@PathVariable("name") String name){
		return String.format("Nice to meet you %s!", name);
	}

	@GetMapping("/welcome")
	public String welcome(@RequestParam(value = "user") String user, @RequestParam(value = "role") String role) {
		String message = "";
		switch (role) {
			case "admin":
				message = String.format("Welcome back to the class portal, Admin %s!", user);
				break;
			case "teacher":
				message = String.format("Thank you for logging in, Teacher %s!", user);
				break;
			case "student":
				message = String.format("Welcome to the class portal, %s!", user);
				break;
			default:
				message = "Role out of range!";
				break;
		}
		return message;
	}

	@GetMapping("/register")
	public String register(@RequestParam(value = "id") String id, @RequestParam(value = "name") String name, @RequestParam(value = "course") String course) {
		Student student = new Student(id, name, course);
		students.add(student);
		return String.format("%s, your id number is registered on the system!", id);
	}

	@GetMapping("/account/{id}")   public String account(@PathVariable("id") String id) {
		for (Student student : students) {
			if (student.getId().equals(id)) {
				return String.format("Welcome back, %s! You are currently enrolled in %s.", student.getName(), student.getCourse());
			}
		}
		return String.format("You provided %s is not found in the system!", id);
	}

	private class Student {
		private String id;
		private String name;
		private String course;
		public Student(String id, String name, String course) {
			this.id = id;
			this.name = name;
			this.course = course;
		}

		public String getId() {
			return id;
		}

		public String getName() {
			return name;
		}

		public String getCourse() {
			return course;
		}
	}

}
